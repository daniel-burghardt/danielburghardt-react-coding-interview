import { useRouter } from 'next/router';
import React, { useEffect, useRef, useState } from 'react';
import { withContextInitialized } from '../../components/hoc';
import {Formik, Form, Field, ErrorMessage} from 'formik';
import * as Yup from 'yup';

const PersonEdit = ({ info, saveInfo }) => {
    const save = (values, actions) => {
        saveInfo({ ...info, ...values });
        actions.setSubmitting(false);
    };

    const validationSchema = Yup.object().shape({
        firstname: Yup.string().required('Required'),
        lastname: Yup.string().required('Required'),
        birthday: Yup.string().required('Required'),
        gender: Yup.string().required('Required'),
    });

    return (
    <>
        <Formik
            initialValues={{
                firstname: info.firstname,
                lastname: info.lastname,
                birthday: info.birthday,
                gender: info.gender,
            }}
            onSubmit={save}
            validationSchema={validationSchema}
        >
            {props => (
                <Form onSubmit={props.handleSubmit}>
                    <Field name="firstname" placeholder="First name" />
                    <ErrorMessage name="firstname" />

                    <Field name="lastname" placeholder="Last name" />
                    <ErrorMessage name="lastname" />

                    <Field type="date" name="birthday" placeholder="Birthday" />
                    <ErrorMessage name="birthday" />

                    <Field name="gender" placeholder="Gender" />
                    <ErrorMessage name="gender" />

                    <button type="submit" disabled={props.isSubmitting}>Submit</button>
                </Form>
            )}
        </Formik>
    </>
  );
};

export default withContextInitialized(PersonEdit);
